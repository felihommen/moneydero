from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField


class Perfil(models.Model):
	user = models.OneToOneField(User)

	@property
	def categorias(self):
		return Categoria.objects.filter(cuentas__titulares=self.user)

	@property
	def cuentas(self):
		return Cuenta.objects.filter(titulares=self.user)

	@property
	def movimientos(self):
		return Movimiento.objects.filter(cuenta__titulares=self.user)

	def __str__(self):
		return self.user.username


class Categoria(models.Model):
	nombre = models.CharField(max_length=50)
	padre  = models.ForeignKey('Categoria', null=True, blank=True)
	cuentas = models.ManyToManyField('Cuenta')

	def __str__(self):
		return self.nombre



class Cuenta(models.Model):
	nombre    = models.CharField(max_length=50)
	titulares = models.ManyToManyField(User)

	def __str__(self):
		return self.nombre



class Movimiento(models.Model):
	user              = models.ForeignKey(User, db_index=True)
	fecha             = models.DateTimeField()
	concepto          = models.CharField(max_length=100)
	categoria         = models.ForeignKey(Categoria)
	cuenta            = models.ForeignKey(Cuenta)
	importe           = models.DecimalField(max_digits=12, decimal_places=2)
	saldo             = models.DecimalField(max_digits=12, decimal_places=2, blank=True)

	class Meta:
		ordering = ('fecha', )

	def guarda(self, user):
		anterior = Movimiento.objects.filter(fecha__lt=self.fecha, user=user, cuenta=self.cuenta).order_by('fecha').last()
		if anterior:
			self.saldo = anterior.saldo + self.importe
		else:
			self.saldo = self.importe
		super().save()
		proximo = Movimiento.objects.filter(fecha__gt=self.fecha, user=user, cuenta=self.cuenta).order_by('fecha').first()
		if proximo:
			proximo.guarda(user)

	@property
	def saldo_global(self, user):
		pass

	def __str__(self):
		return self.concepto
