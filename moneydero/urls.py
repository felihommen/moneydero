from django.conf.urls import patterns, include, url
from django.contrib import admin

from .views import *

urlpatterns = patterns('',
    # Examples:
    url(r'^$', ResumenView.as_view(), name='home'),
    url(r'^resumen/$', ResumenView.as_view(), name='resumen'),
    url(r'^movimientos/$', MovimientosView.as_view(), name='movimientos'),

    url(r'^login/', LoginView.as_view(), name="login"),
    url(r'^admin/', include(admin.site.urls)),
)
