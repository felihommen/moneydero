# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('moneydero', '0001_squashed_0006_auto_20150723_1105'),
    ]

    operations = [
        migrations.AddField(
            model_name='movimiento',
            name='saldo_categoria',
            field=models.DecimalField(blank=True, max_digits=12, default=0, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='movimiento',
            name='saldo_cuenta',
            field=models.DecimalField(blank=True, max_digits=12, default=0, decimal_places=2),
            preserve_default=False,
        ),
    ]
