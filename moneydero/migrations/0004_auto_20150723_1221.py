# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('moneydero', '0003_auto_20150723_1206'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='categoria',
            name='compartido',
        ),
        migrations.RemoveField(
            model_name='categoria',
            name='user',
        ),
        migrations.RemoveField(
            model_name='cuenta',
            name='compartido',
        ),
        migrations.RemoveField(
            model_name='cuenta',
            name='user',
        ),
        migrations.AddField(
            model_name='categoria',
            name='cuenta',
            field=models.ForeignKey(to='moneydero.Cuenta', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cuenta',
            name='titulares',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]
