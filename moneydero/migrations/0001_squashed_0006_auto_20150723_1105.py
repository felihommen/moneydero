# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    replaces = [('moneydero', '0001_initial'), ('moneydero', '0002_auto_20150710_1233'), ('moneydero', '0003_auto_20150710_1234'), ('moneydero', '0004_auto_20150723_1102'), ('moneydero', '0005_auto_20150723_1103'), ('moneydero', '0006_auto_20150723_1105')]

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('nombre', models.CharField(max_length=50)),
                ('padre', models.ForeignKey(to='moneydero.Categoria', null=True, blank=True)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Cuenta',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('nombre', models.CharField(max_length=50)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Movimiento',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('fecha', models.DateTimeField()),
                ('concepto', models.CharField(max_length=100)),
                ('importe', models.DecimalField(max_digits=12, decimal_places=2)),
                ('saldo', models.DecimalField(max_digits=12, blank=True, decimal_places=2)),
                ('categoria', models.ForeignKey(to='moneydero.Categoria')),
                ('cuenta', models.ForeignKey(to='moneydero.Cuenta')),
                ('perfil', models.ForeignKey(to='moneydero.Perfil')),
            ],
        ),
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RenameField(
            model_name='categoria',
            old_name='usuario',
            new_name='perfil',
        ),
        migrations.RemoveField(
            model_name='categoria',
            name='perfil',
        ),
        migrations.RenameField(
            model_name='cuenta',
            old_name='usuario',
            new_name='perfil',
        ),
        migrations.RemoveField(
            model_name='cuenta',
            name='perfil',
        ),
        migrations.AlterModelOptions(
            name='movimiento',
            options={'ordering': ('fecha',)},
        ),
        migrations.AddField(
            model_name='categoria',
            name='compartido',
            field=models.ManyToManyField(to='moneydero.Perfil'),
        ),
        migrations.AddField(
            model_name='categoria',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cuenta',
            name='compartido',
            field=models.ManyToManyField(to='moneydero.Perfil'),
        ),
        migrations.AddField(
            model_name='cuenta',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, default=1),
            preserve_default=False,
        ),
        migrations.RemoveField(
            model_name='movimiento',
            name='perfil',
        ),
        migrations.AddField(
            model_name='movimiento',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, default=1),
            preserve_default=False,
        ),
    ]
