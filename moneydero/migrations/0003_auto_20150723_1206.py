# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('moneydero', '0002_auto_20150723_1200'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='movimiento',
            name='saldo_categoria',
        ),
        migrations.RemoveField(
            model_name='movimiento',
            name='saldo_cuenta',
        ),
    ]
