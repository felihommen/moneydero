# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('moneydero', '0004_auto_20150723_1221'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='categoria',
            name='cuenta',
        ),
        migrations.AddField(
            model_name='categoria',
            name='cuentas',
            field=models.ManyToManyField(to='moneydero.Cuenta'),
        ),
    ]
