from django.contrib import admin
from .models import *

@admin.register(Movimiento)
class MovimientoAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'concepto', 'importe')


admin.site.register(Categoria)
admin.site.register(Cuenta)
admin.site.register(Perfil)
