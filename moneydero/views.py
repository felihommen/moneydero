from django.shortcuts import redirect
from django.views.generic import TemplateView, View, FormView
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from .models import *
from .forms import FormAsiento, FormLogin
from datetime import timedelta
from calendar import monthrange

class BaseView(View):
    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated():
                return super().dispatch(*args, **kwargs)
        return redirect('/login/')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['session'] = self.request.session
        return context

    def post(self, request, *args, **kwargs):
        if 'fecha' in request.POST:
            form_class = self.get_form_class()
            form = self.get_form(form_class)
            if form.is_valid():
                return self.form_valid(form, **kwargs)
            else:
                return self.form_invalid(form, **kwargs)
        if 'categoria' in request.POST:
            try:
                request.session['categoria'] = self.request.user.perfil.categorias.get(pk=request.POST['categoria']).nombre
            except:
                request.session['categoria'] = None
        elif 'periodo' in request.POST:
            if request.POST['periodo'] == 'personalizado':
                pass
            else:
                request.session['periodo'] = request.POST['periodo']
        return redirect(request.path)

    def saldo(self):
        if self.request.session.get('categoria', default='0') == '0':
            saldos = []
            for categoria in self.request.user.categorias:
                if self.request.session.get('periodo', default='Siempre') != 'Siempre':
                    saldo = Movimiento.objects.filter(
                                                perfil=self.request.user.perfil,
                                                categoria=categoria
                                                ).last().saldo
                else:
                    saldo = Movimiento.objects.filter(
                                                perfil=self.request.user.perfil,
                                                categoria=categoria,
                                                fecha__lte=self.fin()
                                                ).last().saldo

    def fin(self):
        periodo = self.request.get('periodo', default="Siempre")
        ahora = timezone.now()
        if periodo == 'Siempre':
            return None
        elif periodo == 'Hoy':
            return ahora.replace(hour=23, minute=59, second=59)
        elif periodo == 'Esta semana':
            faltan = 6 - ahora.weekday()
            return (ahora + timedelta(days=faltan)).replace(hour=23, minute=59, second=59)
        elif periodo == 'Este mes':
            fin = monthrange(ahora.year, ahora.month)[1]
            return ahora.replace(day=fin, hour=23, minute=59, second=59)
        elif periodo == 'Este año':
            return ahora.replace(month=12, day=31, hour=23, minute=59, second=59)
        elif periodo == 'personalizado':
            return None
        return ahora



class ResumenView(BaseView, TemplateView):
    template_name = 'moneydero/resumen.html'



class MovimientosView(BaseView, FormView):
    template_name = 'moneydero/movimientos.html'
    form_class = FormAsiento
    success_url = '/movimientos/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.session.get('categoria', None):
            context['movimientos'] = self.request.user.perfil.movimientos.filter(categoria__nombre=self.request.session['categoria']).order_by('-fecha')
        else:
            context['movimientos'] = self.request.user.perfil.movimientos.order_by('-fecha')
        return context

    def form_valid(self, form):
        if 'signo' in self.request.POST:
            importe = form.cleaned_data['cantidad']
        else:
            importe = -form.cleaned_data['cantidad']
        categoria=Categoria.objects.get(pk=form.cleaned_data['categoria'])
        cuenta=Cuenta.objects.get(pk=form.cleaned_data['cuenta'], titulares=self.request.user)
        ok = Categoria.objects.filter(cuentas=cuenta).exists()
        if ok:
            movimiento = Movimiento(user=self.request.user,
                                    fecha=form.cleaned_data['fecha'],
                                    concepto=form.cleaned_data['concepto'],
                                    categoria=categoria,
                                    cuenta=cuenta,
                                    importe=importe,
            )
            movimiento.guarda(user=self.request.user)
        return super().form_valid(form)


class LoginView(FormView):
    template_name = 'moneydero/login.html'
    form_class = FormLogin
    success_url = '/'

    def form_valid(self, form):
        usuario = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        if usuario is not None:
            if usuario.is_active:
                login(self.request, usuario)
        return super().form_valid(form)
