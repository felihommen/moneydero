from django import forms

class FormAsiento(forms.Form):
    fecha = forms.DateField()
    cuenta = forms.IntegerField()
    categoria = forms.IntegerField()
    concepto = forms.CharField()
    #signo = forms.BooleanField()
    cantidad = forms.DecimalField(decimal_places=2, max_digits=12)


class FormLogin(forms.Form):
    username = forms.CharField()
    password = forms.CharField()
